# IPython Notebooks for 2022 Paper

## Description
These are companion notebooks for the paper [Low-Energy Electron-Track Imaging for a Liquid Argon Time-Projection-Chamber Telescope Concept using Probabilistic Deep Learning](https://arxiv.org/abs/2207.07805) by M. Buuck, A. Mishra, E. Charles, N. Di Lalla, O. Hitchcock, M.E. Monzani, N. Omodei, and T. Shutt. Additional code used to produce the results in this paper will be provided by the authors upon reasonable request. You can find an email address in the paper.

## Installation
These notebooks should run out-of-the-box, as long as the required Python dependencies are installed. Use the provided a `pip` requirements file to set up your environment. Note that you may need to set `core.symlinks` to `true` in your `~/.gitconfig` file to enable proper download of the symlinks in this repository.

## Support
Please email one of the authors of the paper if you have questions on how to use this software. An email address is included in the paper.

## Project status
This repository serves as an archive for data and code presented in [Low-Energy Electron-Track Imaging for a Liquid Argon Time-Projection-Chamber Telescope Concept using Probabilistic Deep Learning](https://arxiv.org/abs/2207.07805) by M. Buuck, A. Mishra, E. Charles, N. Di Lalla, O. Hitchcock, M.E. Monzani, N. Omodei, and T. Shutt. It will not be maintained once the files and data are in place.
